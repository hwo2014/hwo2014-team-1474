var net = require("net");
var linq = require('Linq');
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {  return send({msgType: "join",data: {name: botName, key: botKey}});});

//client = net.connect(serverPort, serverHost, function() { return send({"msgType": "createRace", "data": {"botId": {"name": botName,"key": botKey},
//  "trackName": "suzuka","password": "smipassword","carCount": 2}});});

//client = net.connect(serverPort, serverHost, function() {return send({"msgType": "createRace", "data": {"botId": {"name": botName,"key": botKey},
//  "trackName": "france",
//  "password": "smipassword",
//  "carCount": 2}});});


function send(json) {
//console.log("sent:"+json);
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(msg) {
	if (msg.msgType === 'carPositions') {carPositions(msg);}
	else if (msg.msgType === 'crash') {crash(msg);}
	else if (msg.msgType === 'spawn') {spawn(msg);}
	else if (msg.msgType === 'yourCar') {yourCar(msg);}
	else if (msg.msgType === 'gameInit') {gameInit(msg);}
	else if (msg.msgType === 'gameStart') {gameStart(msg);}
	else if (msg.msgType === 'gameEnd') {gameEnd(msg);}
	else if (msg.msgType === 'join') {yourCar(msg);}
	else if(msg.msgType ==="turboAvailable"){turboAvailable(msg);}
	else if(msg.msgType === 'tournamentEnd'){tournamentEnd(msg);}
	else {send({msgType: "ping",data: {}});}
  }
);

jsonStream.on('error', function() {
  return console.log("disconnected");
});


///////////////////////////////////////////////////////////////////////////////////////////////////////////
function isStraight(trackPiece){ return typeof trackPiece.radius == 'undefined';}
function isCurve(trackPiece){ return typeof trackPiece.length == 'undefined';}

function getSpeed(currentPosition){
if(typeof lastPosition=='undefined') return 0.00;

if(lastPosition.pieceIndex==currentPosition.pieceIndex){
	return currentPosition.inPieceDistance - lastPosition.inPieceDistance;
}
	var lastPiece = trackPieces[lastPosition.pieceIndex];
	var laneOffset = trackLanes[lastPosition.lane.startLaneIndex].distanceFromCenter;
	var lastPositionDistance = isCurve(lastPiece)?2 * Math.PI * (lastPiece.radius+laneOffset) / 360 * Math.abs(lastPiece.angle) : lastPiece.length;
	return currentPosition.inPieceDistance + lastPositionDistance - lastPosition.inPieceDistance;
}

function getNext10Pieces(currentIndex){
	var pieces=[];
	for(var i=0;i<10;i++){
		currentIndex++;
		if(currentIndex>=trackPieces.length)currentIndex=0;
		pieces.push(trackPieces[currentIndex]);
	}
	return pieces;
}

function getNextPiece(currentIndex){
	currentIndex++;
	if(currentIndex>=trackPieces.length)currentIndex=0;
	return trackPieces[currentIndex];
}

function getExtranceSpeed(nextEntranceSpeed,maxSpeed, piece, lane){
	if(nextEntranceSpeed>=maxSpeed) return maxSpeed;

	//var laneOffset = trackLanes[lane].distanceFromCenter;
	var distance = isCurve(piece)?2.00 * Math.PI * (piece.radius+minLaneOffset) / 360 * Math.abs(piece.angle) : piece.length;
	var point = Math.ceil(distance);
	var extranceSpeed = nextEntranceSpeed;
	
	while(point>0){
		var speed = extranceSpeed * deceleration;
		if(speed>maxSpeed){
			extranceSpeed=maxSpeed;
		}else{
			extranceSpeed=speed;
		}
		
		point = point - (Math.ceil(extranceSpeed)*3);
	}
	
	return extranceSpeed;
}

function getIdealSpeed(nextEntranceSpeed,maxSpeed,piece,inPieceDistance,lane){
	if(maxSpeed<=nextEntranceSpeed) return maxSpeed;
	
	var laneOffset = trackLanes[lane].distanceFromCenter;
	var distance = isCurve(piece)? 2.00 * Math.PI * (piece.radius+minLaneOffset) / 360 * Math.abs(piece.angle) : piece.length;
	var point = Math.ceil(distance - inPieceDistance);
	var idealSpeed = nextEntranceSpeed;
	
	//console.log("///////////POint:"+point);
	
	
	
	while(point>0){
		var speed = idealSpeed * deceleration;
		//console.log("///////////speed:"+speed);
		if(speed>maxSpeed){
			idealSpeed=maxSpeed;
		}else{
			idealSpeed=speed;
		}
		//console.log("///////////idealSpeed:"+idealSpeed);
		point = point -  (Math.ceil(idealSpeed)*3);
	}
	
	return idealSpeed;
}

function getShortestLane(switchPiece){
	var curveDistance = 0;
	var startIndex = switchPiece.index;

	for(i=0;i<trackPieces.length;i++){
		startIndex++;
		var nextPiece = getNextPiece(startIndex);
		if(nextPiece.switch) break;
		
		if(isCurve(nextPiece)){
			var laneDistance = 2.00 * Math.PI * (nextPiece.radius) / 360 * nextPiece.angle;
			curveDistance+=laneDistance;
		}
	}
	
	return curveDistance;
}

function calculateSpeed(){
	for(var i=0;i<trackPieces.length;i++){		
		trackPieces[i].index=i;
		trackPieces[i].lastAngle=0.00;
		if(isCurve(trackPieces[i])){
			trackPieces[i].maxSpeed = Math.sqrt(slipForce*trackPieces[i].radius);
		}else{
			trackPieces[i].maxSpeed = 100.00;
		}
	}
}

function learnLastSpeed(currentPosition){
	if(typeof lastPosition=='undefined') return;
	if(lastPosition.pieceIndex==currentPosition.pieceIndex) return;
	
	var lastPiece = trackPieces[lastPosition.pieceIndex];
	if(isStraight(lastPiece)) return;

	if(lastPosition.lastAngle<=51){
		console.log("///////////");
		var factor = ((51-lastPosition.lastAngle)/51*0.05)+1;
		console.log("lastAngle="+lastPosition.lastAngle);
		console.log("factor="+factor);
		console.log("lastSpeed="+lastSpeed);
		var maxSpeed=trackPieces[lastPosition.pieceIndex].maxSpeed;
		var newSpeed=factor*maxSpeed;
		console.log("newSpeed="+newSpeed);	
		trackPieces[lastPosition.pieceIndex].maxSpeed = maxSpeed > newSpeed? maxSpeed:newSpeed;
		//trackPieces[lastPosition.pieceIndex].lastAngle = Math.abs(lastAngle);	
		console.log("///////////learned speed="+trackPieces[lastPosition.pieceIndex].maxSpeed);
	}
	
	if(!hasCrashedBefore){
	var newSlipForce = lastSpeed * lastSpeed / lastPiece.radius;
	slipForce = slipForce>newSlipForce? slipForce:newSlipForce;
	}
}

//////////////////////////////////////////////////////////////////////////////
var carColor;
var trackLanes;
var trackPieces;
var acceleration=0.00;
var deceleration=0.00;
var lastPosition;
var lastAngle=0.00;
var lastThrottle=0.00;
var slipForce=0.30;
var learnedSpeeds=[];
var hasCrashed=false;
var crashedAngleDifference=0.00;
var hasTurbo=false;
var isQualifyingRound=true;
var hasCrashedBefore=false;
var hasSendSwitch=false;
var minLaneOffset=0.0;

function yourCar(msg){
carColor = msg.data.color;
console.log("Car color:" + carColor);
}

function gameInit(msg){
	console.log("Game Init");
	trackLanes = msg.data.race.track.lanes;


	minLaneOffset = Enumerable.From(trackLanes).Min(function (x) { return x.distanceFromCenter });
	console.log("minLaneOffset:"+minLaneOffset);
	trackPieces = msg.data.race.track.pieces;
	if(isQualifyingRound){
		calculateSpeed();
	}
}

function gameStart(msg){
console.log("Game Started");
}

function carPositions(msg){
if(hasCrashed){
send({msgType: "throttle", data:0});
return;
}

var data = Enumerable.From(msg.data).FirstOrDefault(function (x) { return x.id.color === carColor });
var throttle=0.00;
var speed=getSpeed(data.piecePosition);
var currentPiece = trackPieces[data.piecePosition.pieceIndex];
var currentLane = data.piecePosition.lane.startLaneIndex;

if(typeof msg.gameTick == "undefined" && isQualifyingRound){
	throttle = 0;
}else if(msg.gameTick<6  && isQualifyingRound){
	throttle = 0.5;
}else if (msg.gameTick==6  && isQualifyingRound){
	acceleration = (speed - lastSpeed)*2;
	console.log("acceleration-"+acceleration); 
	throttle = 0;
}else if (msg.gameTick<12  && isQualifyingRound){
	throttle = 0;	
}else if (msg.gameTick==12  && isQualifyingRound){
	deceleration = ((lastSpeed-speed) / lastSpeed) + 1.0;
	console.log("deceleration-"+deceleration);
	throttle = 0.5;
}else{	
	var next10Pieces=getNext10Pieces(data.piecePosition.pieceIndex);
	var nextEntranceSpeed=100.00;
	for(var i=9;i>=0;i--){
		var piece=next10Pieces[i];
		nextEntranceSpeed=getExtranceSpeed(nextEntranceSpeed,piece.maxSpeed,piece);	
	}
		
	var idealSpeed = getIdealSpeed(nextEntranceSpeed,currentPiece.maxSpeed,currentPiece,data.piecePosition.inPieceDistance,currentLane)		
	
	//if(idealSpeed>9 && hasTurbo){
	//	send({"msgType": "turbo", "data": "BOOOOOOOOOOOOOOOOOMMMMMMMMMMMMMM"});
	//	hasTurbo=false;
	//}
		
	if(speed <= idealSpeed){
		var factor = 1.10;
		throttle = ((idealSpeed - speed) / acceleration)*factor;
		throttle = (throttle > 1) ? 1:throttle;
	}
	else{
		throttle = 0;				
	}
}

learnLastSpeed(data.piecePosition);
crashedAngleDifference = Math.abs(data.angle)-Math.abs(data.angle);

var lastPositionMaxAngle = 0;
if(typeof lastPosition!='undefined' && lastPosition.pieceIndex==data.piecePosition.pieceIndex){
lastPositionMaxAngle = lastPosition.lastAngle>Math.abs(data.angle)?lastPosition.lastAngle:Math.abs(data.angle);
}

lastPosition = data.piecePosition;
lastPosition.lastAngle=lastPositionMaxAngle;
//console.log("lastPosition:"+lastPosition.lastAngle);
lastAngle = data.angle;
lastSpeed = speed;
lastThrottle = throttle;

if(!hasCrashed)console.log("R:"+currentPiece.radius+" A:" + currentPiece.angle+" S:"+speed+"/t + car:"+ data.angle+" ideal:"+idealSpeed+" nThr:"+throttle);

sendLanes(data.piecePosition);
send({msgType: "throttle", data:throttle});
if(currentPiece.switch){
hasSendSwitch = false;
}
}

function gameEnd(msg){
console.log("Game Ended");
}

function sendLanes(currentPosition){
	var nextPiece = getNextPiece(currentPosition.pieceIndex);
	if(nextPiece.switch && !hasSendSwitch){
		var shortestLane = getShortestLane(nextPiece);	
		
		if(shortestLane>0){
			send({"msgType": "switchLane", "data": "Right"});
			hasSendSwitch = true;
		}else if(shortestLane<0){			
			send({"msgType": "switchLane", "data": "Left"});
			hasSendSwitch = true;
		}
	}
}

function crash(msg){
console.log(msg.data.color +"---Crashed");
if(msg.data.color==carColor){

console.log("Crashed");
hasCrashed=true;
hasCrashedBefore=true;
slipForce = slipForce * 0.70;

console.log("slipForce:"+slipForce);
calculateSpeed();

}
}

function turboAvailable(msg){
hasTurbo=true? msg.data.turboDurationTicks<=30:false;
}

function spawn(msg){
console.log("Spawned");
if(msg.data.color==carColor){
hasCrashed=false;
}

}

function tournamentEnd(msg){
isQualifyingRound=false;
}


